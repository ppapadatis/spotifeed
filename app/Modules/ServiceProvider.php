<?php
namespace Spotifeed\Modules;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $modules = config("modules.modules");
        while (list(,$module) = each($modules)) {
            $routePath = app_path('Modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'routes.php');
            $viewPath = app_path('Modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'Views');
            if (file_exists($routePath)) include $routePath;
            if (is_dir($viewPath)) $this->loadViewsFrom($viewPath, $module);
        }
    }

    public function register() {}
}
