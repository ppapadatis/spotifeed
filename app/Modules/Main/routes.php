<?php

Route::group(array('module' => 'Main', 'namespace' => 'Spotifeed\Modules\Main\Controllers'), function() {

    /** ID Pattern */
    Route::pattern('id', '[0-9]+');

    /** Main Routes */
    Route::get('/', 'MainController@index');
});
