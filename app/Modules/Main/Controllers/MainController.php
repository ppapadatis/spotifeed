<?php
namespace Spotifeed\Modules\Main\Controllers;

use Spotifeed\Http\Requests;
use Spotifeed\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * @return \Response
     */
    public function index()
    {
        return \View('Main::index');
    }
}
