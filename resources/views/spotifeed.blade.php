<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <!-- metas -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <!-- favicon -->
        <link href="{{ asset('favicon.ico') }}" rel="shortcut icon">
        <!-- title -->
        <title>spotifeed - @yield('page-title')</title>
        <!-- stylesheets -->
        <link rel="stylesheet" href="{{ asset('assets/core/semantic.min.css') }}" />
        @yield('top-css-files')
        <link rel="stylesheet" href="{{ asset('assets/extra/css/global.css') }}" />
        <!-- header javascript -->
        @yield('top-js-files')
    </head>
    <body>
        @yield('content')
        <!-- scripts -->
        <script src="http://code.jquery.com/jquery-2.2.2.min.js"></script>
        <script src="{{ asset('assets/core/semantic.min.js') }}"></script>
        @yield('bottom-js-files')
        <script src="{{ asset('assets/extra/js/global.js') }}"></script>
    </body>
</html>
